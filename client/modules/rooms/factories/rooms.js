var main = angular.module('main');

main.factory('Rooms',
  function (CollectionResource) {
    return CollectionResource(Rooms);
  }
)
