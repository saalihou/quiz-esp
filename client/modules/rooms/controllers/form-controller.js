var main = angular.module('main');

function RoomFormController($scope, Rooms) {
  $scope.reset = function () {
    $scope.room = {};
  }
  if (!$scope.room) {
    $scope.reset();
  }

  $scope.save = function (room) {
    Rooms.save(room);
  }
}
main.controller('RoomFormController', RoomFormController);
