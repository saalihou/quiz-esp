var main = angular.module('main');

function RoomsController ($scope, Rooms, $modal) {
  $scope.rooms = Rooms.query();

  $scope.remove = function (room) {
    Rooms.remove(room);
  }
}
main.controller('RoomsController', RoomsController);
