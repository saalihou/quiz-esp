var main = angular.module('main');

function QuestionFormController($scope, Questions) {
  $scope.reset = function () {
    $scope.question = {
      type: 'mcq-one',
      responses: []
    };
  }
  if (!$scope.question) {
    $scope.reset();
  }

  $scope.save = function (question) {
    Questions.save(question);
  }

  $scope.addResponse = function (response) {
    var responses = $scope.question.responses;
    if (!response || !response.name || !response.name.length) {
      return;
    }
    response.name = response.name.toLowerCase();
    responses.push(response);
  }

  $scope.setValidResponse = function (response) {
    var question = $scope.question,
      responses = question.responses;

    var type = question.type;
    if (type !== 'mcq-one' && type !== 'tf') {
      return;
    }
    responses.forEach(function (response) {
      response.valid = false;
    });

    response.valid = true;
  }

  var responsesRef;
  $scope.$watchCollection('question.type', function (type, previousType) {
    var question = $scope.question;
    if (/mcq/.test(previousType)) {
      responsesRef = angular.copy(question.responses);
    }
    if (type === 'tf') {
      question.responses = [
        {
          name: 'vrai',
          valid: true
        },
        {
          name: 'faux',
          valid: false
        }
      ];
    } else if (type === 'typed-response') {
      question.responses = [{
        name: '',
        valid: true
      }];
    } else {
      if (responsesRef) {
        question.responses = responsesRef;
      }
    }
  });
}
main.controller('QuestionFormController', QuestionFormController);
