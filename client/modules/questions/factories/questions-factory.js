var main = angular.module('main');

main.factory('Questions',
  function (CollectionResource) {
    return CollectionResource(Questions);
  }
);
