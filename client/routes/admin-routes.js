var main = angular.module('main');

main.config(function ($stateProvider) {
  $stateProvider
    .state('admin', {
      url: '/admin',
      abstract: true,
      templateUrl: 'client/admin-template.html'
    })
    .state('admin.questions', {
      url: '/questions',
      templateUrl: 'client/modules/questions/views/admin-template.html'
    })
    .state('admin.rooms', {
      url: '/rooms',
      templateUrl: 'client/modules/rooms/views/admin-template.html'
    });
});
