Questions = new Mongo.Collection('questions');

var ResponseSchema = new SimpleSchema({
  name: {
    type: String
  },
  valid: {
    type: Boolean
  }
});

var QuestionSchema = new SimpleSchema({
  prompt: {
    type: String,
    min: 10
  },
  type: {
    type: String,
    allowedValues: ['mcq-one', 'mcq-multiple', 'typed-response', 'tf']
  },
  timeout: {
    type: Number
  },
  responses: {
    type: [ResponseSchema]
  }
});

Questions.attachSchema(QuestionSchema);
