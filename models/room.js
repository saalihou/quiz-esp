Rooms = new Mongo.Collection('rooms');

var ParticipantSchema = new SimpleSchema({
  name: {
    type: String,
    min: 2
  },
  score: {
    type: Number,
    defaultValue: 0
  }
});

var QuestionSchema = new SimpleSchema({
  id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  index: {
    type: Number,
    min: 0
  }
})

var RoomSchema = new SimpleSchema({
  name: {
    type: String,
    min: 3,
    max: 100
  },
  participants: {
    type: [ParticipantSchema],
    defaultValue: []
  },
  pendingParticipants: {
    type: [ParticipantSchema],
    defaultValue: []
  },
  questions: {
    type: [QuestionSchema],
    defaultValue: []
  }
});

Rooms.attachSchema(RoomSchema);
